FROM apache/superset

USER root
RUN mkdir -p /coder/apps && \
    apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y \
      bash \
      zsh \
      build-essential \
      ca-certificates \
      curl \
      sudo \
      htop \
      software-properties-common \
      unzip \
      vim \
      wget \
      git && \
    echo "superset ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/nopasswd

COPY ./config.yaml ./superset3.png /coder/apps/

USER superset

RUN superset db upgrade 
RUN superset fab create-admin --username admin \
               --firstname Superset \
               --lastname Admin \
               --email admin@superset.com \
               --password admin 
